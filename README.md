# Run Backend
    $ cd backend
    $ npm install
    $ npm start

# Run Frontend
    $ cd frontend
    $ npm install
    $ npm start

# 1.List Products
    create products array
    add product images
    render products
    style products
# 2. Add page routing
    npm i react-router-dom
    create route for home screen
    create router for product screen

# 3. Create Node.JS Server
    run npm init in root folder
    Update package.json set type: module
    Add .js to imports
    npm install express
    create server.js
    add start command as node backend/server.js
    require express
    create route for / return backend is ready.
    move products.js from frontend to backend
    create route for /api/products
    return products
    run npm start
# 4. Fetch Products From Backend
    set proxy in package.json
    npm install axios
    use state hook
    use effect hook
    use reducer hook
# 5. Manage State By Reducer Hook
    define reducer
    update fetch data
    get state from usReducer
# 6. Add bootstrap UI Framework
    npm install react-bootstrap bootstrap
    update App.js

# 7. Create Product and Rating Component
    create Rating component
    Create Product component
    Use Rating component in Product component
    
# 8. Create Product Details Screen
    fetch product from backend
    create 3 columns for image, info and action

 # 9. Create Loading and Message Component
     create loading component
     use spinner component
     craete message component
     create utils.js to define getError fuction

# 10.Create React Context For Add Item To Cart
    Create React Context
    define reducer
    create store provider
    implement add to cart button click handler

# 11. Complete Add To Cart
     check exist item in the cart
     check count in stock in backend

# 12.Create Cart Screen
    create 2 columns
    display items list
    create action column

# 13. Complete Cart Screen
    click handler for inc/dec item
    click handler for remove item
    click handler for checkout
# 14. Create Signin Screen
    create sign in form
    add email and password
    add signin button

# 15 .Connect To MongoDB Database
    create atlas monogodb database
    install local mongodb database
    npm install mongoose
    connect to mongodb database

# 16. Seed Sample Products
    create Product model
    create seed route
    use route in server.js
    seed sample product

# 17. Seed Sample Users
    create user model
    seed sample users
    create user route 

# 18. Create Signin Backend API
    create signin api
    npm install jsonwebtoken
    define generateToken

# 19. Complete Signin Screen
    handle submit action
    save token in store and local storage
    show user name in header

# 20. Create Shipping Screen
    create form inputs
    handle save shipping address
    add checkout wizard bar

# 21. Create Sign Up Screen
    create input forms
    handle submit
    create backend api

# 22. Implement Select Payment Method Screen
    create input forms
    handle submit

# 23. Create Place Order Screen
    show cart items, payment and address
    handle place order action
    create order create api

# 24. Implement Place Order Action
     handle place order action
     create order create api

# 25. Create Order Screen
    create backend api for order/:id
    fetch order api in frontend
    show order information in 2 cloumns
# 26. Pay Order By PayPal
    generate paypal client id
    create api to return client id
    install react-paypal-js
    use PayPalScriptProvider in index.js
    use usePayPalScriptReducer in Order Screen
    implement loadPaypalScript function
    render paypal button
    implement onApprove payment function
    create pay order api in backend

# 27. Display Order History
     create order screen
     create order history api
     use api in the frontend

# 28. Create Profile Screen
     get user info from context
     show user information
     create user update api
     update user info

# 29. Publish To Heroku
     create and config node project
     serve build folder in frontend folder
     Create heroku account
     connect it to github
     Create mongodb atlas database
     Set database connection in heroku env variables
     Commit and push

 # 30. Add Sidebar and Search Box
     add sidebar
     add search box

# 31. Create Search Screen
     show filters
     create api for searching products
     display results

# 32. Create Admin Menu
     define protected route component
     define admin route component
     add menu for admin in header

# 33. Create Dashboard Screen
     create dashboard ui
     implement backend api
     connect ui to backend

# 34. Manage Products
     create products list ui
     implement backend api
     fetch data

# 35. Create Product
     create products button
     implement backend api
     handle on click

# 36. Create Edit Product
     create edit button
     create edit product ui
     dispaly product info in the input boxes

# 37. Implement Update Product
     create edit product backend api
     handle update click

# 38. Upload Product Image
     create cloudinary account
     use the api key in env file
     handle upload file
     implement backend api to upload  

# 39. Delete Product
     show delete button
     implement backend api
     handle on click

# 40. List Orders
     create order list screen
     implement backend api
     fetch and display orders

# 41. Deliver Order
     add deliver button 
     handle click action
     implement backend api for deliver

# 42. Delete Order
     add delete button
     handle click action
     implement backend api for delete

 # 43. List Users
     create user list screen
     implement backend api
     fetch and display users

# 44. Edit User
     create edit button
     create edit product ui
     display product info in the input boxes
     implement backend api
     handle edit click

# 45. Delete User
      add delete button
      handle click action
      implement backend api for delete

# 46. Review Orders
     create submit review form
     handle submit
     implement backend api for review

# 47. Upload multiple Images
     add images to product model
     get images in edit screen
     show images in product screen

# 48. Implement Live Chat With Customers
       use socket io to create backend
       create chat box component
       create support screen

