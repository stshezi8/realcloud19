import http from 'http';
import { Server } from 'socket.io';
import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import seedRouter from './routes/seedRoutes.js';
import productRouter from './routes/productRoutes.js';
import userRouter from './routes/userRoutes.js';
import orderRouter from './routes/orderRoutes.js';
import uploadRouter from './routes/uploadRoutes.js';

// Fetch veriables from .env file 
dotenv.config();

//It's time to connect to Mongo DB 
mongoose.connect(process.env.MONGODB_URI)
// This connection string return a promice that is why we use then 
  .then(() => {
    console.log('connected to db');// If everything is Ok then console log connected to db
  })
  .catch((err) => {
    console.log(err.message); // Else error 
  });

// This instance will return the object of express 
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//This api will return client id if it's doesn't exist it will return sb(send box)
app.get('/api/keys/paypal', (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID || 'sb');
});

// If we are calling this path , the path in Router /SeedRoutes will be called 
app.use('/api/upload', uploadRouter);
app.use('/api/seed', seedRouter);
app.use('/api/products', productRouter);
app.use('/api/orders', orderRouter);


app.use('/api/users', userRouter);
// Setting up an error of 500 from server 
app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});
 
// process.env.PORT access to free port , if it doesn't exist then use port 6000
const port = process.env.PORT || 6000;
// Starting the server , first parameter is the port 2nd is the function that will run when the server is ready 
// app.listen(port, () => {
//  console.log(`serve at http://localhost:${port}`);
// });

// Create Http Server 
const httpServer = http.Server(app);
// Create Soket io 
const io = new Server(httpServer, { cors: { origin: '*' } });
const users = [];

// Defining socket.io event , Name of the event is connection 
io.on('connection', (socket) => {
  console.log('connection', socket.id);
  // This one will run when the user close the connection or browser 
  socket.on('disconnect', () => {
    // First we chack if the user Exist 
    const user = users.find((x) => x.socketId === socket.id);
    if (user) {
      //If yes we make that user Offline 
      user.online = false;
      console.log('Offline', user.name);
      //If we have Admin and the Admin is online 
      const admin = users.find((x) => x.isAdmin && x.online);
      if (admin) {
        //We have to update the Admin about the status  of that user 
        io.to(admin.socketId).emit('updateUser', user);
      }
    }
  });

  // This even will run when we have a new user Online 
  socket.on('onLogin', (user) => {
    const updatedUser = {
      ...user,
      online: true,// Set Online to true 
      socketId: socket.id,
      messages: [],
    };
    // Check if the user exist 
    const existUser = users.find((x) => x._id === updatedUser._id);
    if (existUser) {
      existUser.socketId = socket.id;
      existUser.online = true;
    } else {
      users.push(updatedUser);
    }
    //Log the user as Online 
    console.log('Online', user.name);
    const admin = users.find((x) => x.isAdmin && x.online);
    if (admin) {
      // Update the admin that this user is online 
      io.to(admin.socketId).emit('updateUser', updatedUser);
    }
    if (updatedUser.isAdmin) {
      io.to(updatedUser.socketId).emit('listUsers', users);
    }
  });
//Get amdnin user and make sure that Admin user is Online 
  socket.on('onUserSelected', (user) => {
    const admin = users.find((x) => x.isAdmin && x.online);
    if (admin) {
      const existUser = users.find((x) => x._id === user._id);
      io.to(admin.socketId).emit('selectUser', existUser);
    }
  });
// On Message event This event happens when there is a new message entered by Admin or User 
  socket.on('onMessage', (message) => {
    //If the sender is Admin 
    if (message.isAdmin) {
      // If reciever exist and online 
      const user = users.find((x) => x._id === message._id && x.online);
      if (user) {
        //Send this message to the user 
        io.to(user.socketId).emit('message', message);
        user.messages.push(message);
      }
    } else {
      //Check if Admin is Online 
      const admin = users.find((x) => x.isAdmin && x.online);
      if (admin) {
        // Send Message to Adnin 
        io.to(admin.socketId).emit('message', message);
        const user = users.find((x) => x._id === message._id && x.online);
        user.messages.push(message);
      } else {
        //Normal User Admin is Offline . 
        io.to(socket.id).emit('message', {
          name: 'Admin',
          //This is the default message that will be sent to the user when Admin is not online 
          body: 'Sorry. I am not online right now',
        });
      }
    }
  });
});

httpServer.listen(port, () => {
  console.log(`serve at http://localhost:${port}`);
});
