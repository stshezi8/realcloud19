import express, { Router } from 'express';
import Product from '../models/productModel.js';
import User from '../models/userModel.js';
import data from '../data.js';

// Creating Router from express 
const seedRouter = express.Router();

// Reason for this is to generate sample Data ,from Data .json to Mongo DB
seedRouter.get('/', async (req, res) => {
 // First we are removing all current products 
    await Product.remove({});
    // We import all froducts fro data.js and store if to createdProducts
  const createdProducts = await Product.insertMany(data.products);
  //Time to send those products to back end 
  await User.remove({});
  const createdUsers = await User.insertMany(data.users);
  res.send({ createdProducts, createdUsers });
});
export default seedRouter;