import express from 'express';
import expressAsyncHandler from 'express-async-handler';
import Order from '../models/orderModel.js';
import User from '../models/userModel.js';
import Product from '../models/productModel.js';
import { isAuth, isAdmin } from '../utils.js';

const orderRouter = express.Router();

// Admin view Order get all orders 
orderRouter.get(
  '/',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const orders = await Order.find().populate('user', 'name');
    res.send(orders);
  })
);

//Get all Orders 
orderRouter.post(
  '/',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const newOrder = new Order({
        // We are looping throug product array and giving each product Unique ID 
      orderItems: req.body.orderItems.map((x) => ({ ...x, product: x._id })),
      shippingAddress: req.body.shippingAddress,
      paymentMethod: req.body.paymentMethod,
      itemsPrice: req.body.itemsPrice,
      shippingPrice: req.body.shippingPrice,
      taxPrice: req.body.taxPrice,
      totalPrice: req.body.totalPrice,
      user: req.user._id,// isAuth Will get us this user Id  
    });

    const order = await newOrder.save();
    res.status(201).send({ message: 'New Order Created', order });
  })
);


orderRouter.get(
  '/summary',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    // to learn more about aggregation : https://www.mongodb.com/docs/manual/aggregation/
    const orders = await Order.aggregate([
      {
        $group: {
          _id: null,// group all ID 
          numOrders: { $sum: 1 },// calculate all object 
          totalSales: { $sum: '$totalPrice' },// Because we are calculating total price we use $totalPrice
        },
      },
    ]);

    // In User collection calculate the number of users 
    const users = await User.aggregate([
      {
        $group: {
          _id: null,
          numUsers: { $sum: 1 },
        },
      },
    ]);

    // For sales Chart 
    const dailyOrders = await Order.aggregate([
      {
        $group: {
          // we use dateToString function to format create date 
          _id: { $dateToString: { format: '%Y-%m-%d', date: '$createdAt' } },
          // Get number of orders in that day 
          orders: { $sum: 1 },
          //Total price for that day 
          sales: { $sum: '$totalPrice' },
        },
      },
      // sort based on the ID 
      { $sort: { _id: 1 } },
    ]);

    // Categories 
    const productCategories = await Product.aggregate([
      {
        $group: {
          _id: '$category',// group by category
          count: { $sum: 1 }, //count number of each category
        },
      },
    ]);
    res.send({ users, orders, dailyOrders, productCategories });
  })
);


// If you put this after Get Order By ID  then Get Order By ID  will handle this functionality 
orderRouter.get(
  '/mine',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const orders = await Order.find({ user: req.user._id });
    res.send(orders);
  })
);

// Get Order By ID 
orderRouter.get(
  '/:id',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (order) {
      res.send(order);
    } else {
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);


orderRouter.put(
  '/:id/deliver',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (order) {
      order.isDelivered = true;
      order.deliveredAt = Date.now();
      await order.save();
      res.send({ message: 'Order Delivered' });
    } else {
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);

// Pay Pal use this to find order by Id and update the status to pay 
orderRouter.put(
  '/:id/pay',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    // Find the order by ID 
    const order = await Order.findById(req.params.id);
    // If the order Exist 
    if (order) {
      // Set isPaid to true 
      order.isPaid = true;
      order.paidAt = Date.now();
      order.paymentResult = {
        id: req.body.id,
        status: req.body.status,
        update_time: req.body.update_time,
        email_address: req.body.email_address,
      };
      // Save the Order and send back the message Order Paid 
      const updatedOrder = await order.save();
      res.send({ message: 'Order Paid', order: updatedOrder });
    } else {
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);

// Admin Delete order 
orderRouter.delete(
  '/:id',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (order) {
      await order.remove();
      res.send({ message: 'Order Deleted' });
    } else {
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);

export default orderRouter;