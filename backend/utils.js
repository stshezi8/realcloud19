import jwt from 'jsonwebtoken';

// This function accept the user Object 
export const generateToken = (user) => {
    //We call sign functions from jwt library that we just installed , 
  return jwt.sign(
      // This function takes 2 parameters First User information
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    },
    // Second is the Encryprion string 
    process.env.JWT_SECRET,
    {
        // This Tocken will expires in 30 days 
      expiresIn: '30d',
    }
  );
};

export const isAuth = (req, res, next) => {
  // Get Authorization from Headers 
  const authorization = req.headers.authorization;
  // If authorization is present 
  if (authorization) {
    // then get the token , slice(7 it get read of Bearer  and only get the Tocken value 
    const token = authorization.slice(7, authorization.length); // Bearer XXXXXX
    // Is is the function that is verifying the Token 
    jwt.verify(token, process.env.JWT_SECRET, (err, decode) => {
      if (err) {
        res.status(401).send({ message: 'Invalid Token' });
      } else {
        req.user = decode;
        next();
      }
    });
  } else {
    // If there is no Authorization there is no Token 
    res.status(401).send({ message: 'No Token' });
  }
};



export const isAdmin = (req, res, next) => {
  // Check if user is valid and user is admin 
  if (req.user && req.user.isAdmin) {
    // then go to next modle ware 
    next();
  } else {
    //Otherwise Show this error message 
    res.status(401).send({ message: 'Invalid Admin Token' });
  }
};