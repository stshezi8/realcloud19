const data = {
    products:[
        {
            _id: '1',
            name: 'Nike SLim Shirt',
            slug: 'Nike SLim Shirt',
            category: 'Shirt',
            image: '/images/p1.jpeg',
            price: 120,
            countInStock: 10,
            brand: 'Nike',
            rating: 5,
            numReviews: 10,
            description: 'high quality product'
        },
        {
            _id: '2',
            name: 'Adidas SLim Shirt',
            slug: 'Adidas SLim Shirt',
            category: 'Shirt',
            image: '/images/p2.jpeg',
            price: 100,
            countInStock: 20,
            brand: 'Adidas',
            rating: 4.5,
            numReviews: 10,
            description: 'high quality product'
        },
        {
            _id: '3',
            name: 'Lacoster Free Shirt',
            slug: 'Lacoster Free Shirt',
            category: 'Shirt',
            image: '/images/p3.jpeg',
            price: 150,
            countInStock: 0,
            brand: 'Lacoster',
            rating: 3.5,
            numReviews: 10,
            description: 'high quality product'
        },
        {
            _id: '4',
            name: 'Puma SLim Shirt',
            slug: 'Puma SLim Shirt',
            category: 'Shirt',
            image: '/images/p4.jpeg',
            price: 65,
            countInStock: 15,
            brand: 'Puma',
            rating: 3,
            numReviews: 10,
            description: 'high quality product'
        },
        {
            _id: '5',
            name: 'Kappa Shirt',
            slug: 'Kappa Shirt',
            category: 'Shirt',
            image: '/images/p5.jpeg',
            price: 110,
            countInStock: 5,
            brand: 'Kappa',
            rating: 2.5,
            numReviews: 10,
            description: 'high quality product'
        },
        {
            _id: '6',
            name: 'Levis',
            category: 'Shirt',
            slug :'Shirt',
            image: '/images/p6.jpeg',
            price: 120,
            countInStock: 12,
            brand: 'Levis',
            rating: 4.5,
            numReviews: 10,
            description: 'high quality product'
        },
    ],
};

export default data;