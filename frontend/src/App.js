// import data from "./data";
import {BrowserRouter, Link, Route, Routes} from "react-router-dom"
import {toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import Navbar from 'react-bootstrap/Navbar';
import Badge from 'react-bootstrap/Badge';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';
import {LinkContainer} from 'react-router-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { Store } from './Store';
import CartScreen from "./screens/CartScreen";
import SigninScreen from './screens/SigninScreen';
import ShippingAddressScreen from './screens/ShippingAddressScreen';
import SignupScreen from './screens/SignupScreen';
import PaymentMethodScreen from './screens/PaymentMethodScreen';
import PlaceOrderScreen from './screens/PlaceOrderScreen';
import OrderScreen from './screens/OrderScreen';
import OrderHistoryScreen from './screens/OrderHistoryScreen';
import ProfileScreen from './screens/ProfileScreen';
import Button from 'react-bootstrap/Button';
import { getError } from './utils';
import axios from 'axios';
import SearchBox from './components/SearchBox';
import SearchScreen from './screens/SearchScreen';
import ProtectedRoute from './components/ProtectedRoute';
import DashboardScreen from './screens/DashboardScreen';
import AdminRoute from './components/AdminRoute';
import ProductListScreen from './screens/ProductListScreen';
import ProductEditScreen from './screens/ProductEditScreen';
import OrderListScreen from './screens/OrderListScreen';
import UserListScreen from './screens/UserListScreen';
import UserEditScreen from './screens/UserEditScreen';
import SupportScreen from './screens/SupportScreen';
import ChatBox from './components/ChatBox';

function App() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  // From this State we are getting , cart , userInfo
  const { cart, userInfo } = state;
//Signout is deleting the user from local storage and call USER_SIGNOUT Action from Strore
  const signoutHandler = () => {
    ctxDispatch({ type: 'USER_SIGNOUT' });
    localStorage.removeItem('userInfo');
    localStorage.removeItem('shippingAddress');
    localStorage.removeItem('paymentMethod');
    window.location.href = '/signin';
  };

// Define the state of sidebarIsOpen Default value is false 
  const [sidebarIsOpen, setSidebarIsOpen] = useState(false);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const { data } = await axios.get(`/api/products/categories`);
        setCategories(data);
      } catch (err) {
        toast.error(getError(err));
      }
    };
    fetchCategories();
  }, []);

  return (
    <BrowserRouter>
    {/* If side bar is open renter first css class otherwise 2nd css class  */}
      <div className={ sidebarIsOpen ? 'd-flex flex-column site-container active-cont' : 'd-flex flex-column site-container'  } >
        {/* We are going to show one toast at a time */}
      <ToastContainer position="bottom-center" limit={1} />
        <header className="App-header">
        {/* Now we are using React bootstrap that we just installed, Convert our simple header to bootstrap Header */}
          <Navbar bg="dark" variant="dark" expand="lg">
            <Container>
            {/* Setting up Toggle */}
            <Button variant="dark" onClick={() => setSidebarIsOpen(!sidebarIsOpen)} >
                <i className="fas fa-bars"></i>
              </Button>

              <LinkContainer to='/'>
                <Navbar.Brand>Cloud19</Navbar.Brand>
              </LinkContainer>
              
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
              <SearchBox />
                <Nav className="me-auto  w-100  justify-content-end">
                  <Link to="/cart" className="nav-link">
                    Cart
                    {cart.cartItems.length > 0 && (
                      <Badge pill bg="danger">
                        {cart.cartItems.reduce((a, c) => a + c.quantity, 0)}
                      </Badge>
                    )}

                </Link>
                {/* If user infor exist then show this dropdown othersise signin  */}
                  {userInfo ? (
                    <NavDropdown title={userInfo.name} id="basic-nav-dropdown">
                      <LinkContainer to="/profile">
                        <NavDropdown.Item>User Profile</NavDropdown.Item>
                      </LinkContainer>
                      <LinkContainer to="/orderhistory">
                        <NavDropdown.Item>Order History</NavDropdown.Item>
                      </LinkContainer>
                      <NavDropdown.Divider />
                      <Link
                        className="dropdown-item"
                        to="#signout"
                        onClick={signoutHandler}
                      >
                        Sign Out
                      </Link>
                    </NavDropdown>
                  ) : (
                    <Link className="nav-link" to="/signin">
                      Sign In
                    </Link>
                  )}

                    {/* If user infor exist and user infor is Admin then show this dropdown  */}
                  {userInfo && userInfo.isAdmin && (
                    <NavDropdown title="Admin" id="admin-nav-dropdown">
                      <LinkContainer to="/admin/dashboard">
                        <NavDropdown.Item>Dashboard</NavDropdown.Item>
                      </LinkContainer>
                      <LinkContainer to="/admin/products">
                        <NavDropdown.Item>Products</NavDropdown.Item>
                      </LinkContainer>
                      <LinkContainer to="/admin/orders">
                        <NavDropdown.Item>Orders</NavDropdown.Item>
                      </LinkContainer>
                      <LinkContainer to="/admin/users">
                        <NavDropdown.Item>Users</NavDropdown.Item>
                      </LinkContainer>

                      <LinkContainer to="/support">
                      <NavDropdown.Item>Support</NavDropdown.Item>
                    </LinkContainer>

                    </NavDropdown>
                  )}

                </Nav>
              </Navbar.Collapse>

            </Container>
          </Navbar>

        </header>

            {/* If Navbar is open /close if it is closed open  */}
        <div className={
            sidebarIsOpen
              ? 'active-nav side-navbar d-flex justify-content-between flex-wrap flex-column'
              : 'side-navbar d-flex justify-content-between flex-wrap flex-column'
          }
        >
          {/* Set Navigation bar of side bar  */}
          <Nav className="flex-column text-white w-100 p-2">
            <Nav.Item>
              <strong>Categories</strong>
            </Nav.Item>
            {categories.map((category) => (
              <Nav.Item key={category}>
                <LinkContainer
                  to={`/search?category=${category}`}
                  onClick={() => setSidebarIsOpen(false)}
                >
                  <Nav.Link>{category}</Nav.Link>
                </LinkContainer>
              </Nav.Item>
            ))}
          </Nav>
        </div>

        <main>
          <Container className="mt-3">
            <Routes>
              {/* For route we need to define 2 atributes first is the path a user put in the browser */}
              {/* Second is the component that react to this path */}
              <Route path="/" element={<HomeScreen />} />
              {/* to define parameter we use : then the parameter  */}
              <Route path="/product/:slug" element={<ProductScreen/>} />
              {/* Search Screen  */}
              <Route path="/search" element={<SearchScreen />} />
              {/* Route for Cart */}
              <Route path="/cart" element={<CartScreen />} />
              {/* Sign Screen Router  */}
              <Route path="/signin" element={<SigninScreen />} />
              {/* Shipping Screen  */}
              <Route path="/shipping" element={<ShippingAddressScreen />} ></Route>
              {/* profile  Screen */}
              <Route path="/profile" element={
                  <ProtectedRoute>
                    <ProfileScreen />
                  </ProtectedRoute>
                }/>
              {/* Signup Screen */}
              <Route path="/signup" element={<SignupScreen />} />
              {/* Payment Screen */}
              <Route path="/payment" element={<PaymentMethodScreen />}></Route>
              {/* placeorder Screen  */}
              <Route path="/placeorder" element={<PlaceOrderScreen />} />
              {/* OrderScreen */}
              <Route path="/order/:id" element={
                  <ProtectedRoute>
                    <OrderScreen />
                  </ProtectedRoute>
                }>
              </Route>

               {/* Admin Routes */}
               <Route path="/admin/dashboard" element={
                  <AdminRoute>
                    <DashboardScreen />
                  </AdminRoute>
                }></Route>

              {/* orderhistory Screen */}
              <Route path="/orderhistory" element={
                  <ProtectedRoute>
                    <OrderHistoryScreen />
                  </ProtectedRoute>
                }>
              </Route>

                {/* Admin Products  */}
              <Route path="/admin/products" element={
                  <AdminRoute>
                    <ProductListScreen />
                  </AdminRoute>
                }>
              </Route>

              {/* Admin Product Edit  */}
              <Route path="/admin/product/:id" element={
                  <AdminRoute>
                    <ProductEditScreen />
                  </AdminRoute>
                }>
              </Route>

              {/* /admin/orders */}
              <Route path="/admin/orders"  element={
                  <AdminRoute>
                    <OrderListScreen />
                  </AdminRoute>
                } >
              </Route>

                {/* ADMIN List of users  */}
              <Route path="/admin/users" element={
                  <AdminRoute>
                    <UserListScreen />
                  </AdminRoute>
                }>
              </Route>

              {/* Admin Edit User  */}
              <Route path="/admin/user/:id" element={
                  <AdminRoute>
                    <UserEditScreen />
                  </AdminRoute>
                } >
              </Route>

              {/* Admin Routes */}
              <Route path="/support" element={
                  <AdminRoute>
                    <SupportScreen />
                  </AdminRoute>
                }></Route>

            </Routes>
          </Container>
        </main>

        <footer >
          {userInfo && !userInfo.isAdmin && <ChatBox userInfo={userInfo} />}
          <div className="text-center">All right reserved</div>{' '}
        </footer>
      </div>
    </BrowserRouter>
  );
}
export default App;
