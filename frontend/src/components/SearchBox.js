import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import { useNavigate } from 'react-router-dom';

export default function SearchBox() {
  const navigate = useNavigate();
//   This is the query Is the data that user enter in the input box 
  const [query, setQuery] = useState('');
  const submitHandler = (e) => {
    e.preventDefault();
    // if query(useState) exist redirect the user to /search/?query=${query} otherwise redirect the user to search screen 
    navigate(query ? `/search/?query=${query}` : '/search');
  };

  return (
    <Form className="d-flex me-auto" onSubmit={submitHandler}>
      <InputGroup>
      {/* Default values in the input box is q  */}
        <FormControl type="text" name="q"cid="q"
        //   When user chnage the input box update the query to new input value 
          onChange={(e) => setQuery(e.target.value)}
          placeholder="search products..."
          aria-label="Search Products"
          aria-describedby="button-search"
        ></FormControl>

        {/* Button we connect the Button with form using button-search */}
        <Button variant="outline-primary" type="submit" id="button-search">
          <i className="fas fa-search"></i>
        </Button>
      </InputGroup>
    </Form>
  );
}