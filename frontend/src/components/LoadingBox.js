import Spinner from 'react-bootstrap/Spinner';

export default function LoadingBox(){
    return (
        // We are trying to show this spin animation from react bootstrap , it the spin has a problem then we show a loading div 
        <Spinner animation="border" role="status">
            <span className="visually-hidden">Loadin...</span>
        </Spinner>
    )
}