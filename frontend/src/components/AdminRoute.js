import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Store } from '../Store';

export default function AdminRoute({ children }) {
  const { state } = useContext(Store);
  const { userInfo } = state;
//   If userInfo exist and userInfo is admin then redirect user to protected section  otherwise redirect to signin 
  return userInfo && userInfo.isAdmin ? children : <Navigate to="/signin" />;
}