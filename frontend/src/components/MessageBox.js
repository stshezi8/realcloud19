import Alert from 'react-bootstrap/Alert'

export default function MessageBox(props){
    return (
        // If the variant exist use the variant that is passed by the user as props otherwise use info as a default and 
        //{props.children} render the content inside the children on the alert 
        <Alert variant={props.variant || 'info'}>{props.children}</Alert>
    )
}