import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Store } from '../Store';

export default function ProtectedRoute({ children }) {
    // We get userInfo from  React context 
  const { state } = useContext(Store);
  const { userInfo } = state;
  //If user infor exist return Children otherwise navigate to signin screen 
  return userInfo ? children : <Navigate to="/signin" />;
}