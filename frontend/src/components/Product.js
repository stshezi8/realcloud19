import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";
import Rating from './Rating';
import axios from 'axios';
import { useContext } from 'react';
import { Store } from '../Store';


function Product(props){
    const {product} = props;

    const { state, dispatch: ctxDispatch } = useContext(Store);
    const {
        cart: { cartItems },
    } = state;

    const addToCartHandler = async (item) => {
        const existItem = cartItems.find((x) => x._id === product._id);
        const quantity = existItem ? existItem.quantity + 1 : 1;
        const { data } = await axios.get(`/api/products/${item._id}`);
        if (data.countInStock < quantity) {
        window.alert('Sorry. Product is out of stock');
        return;
        }
        ctxDispatch({
        type: 'CART_ADD_ITEM',
        payload: { ...item, quantity },
        });
    };

    return (
        // Because we are using a single page application framework, If we are navigating between pages we don't have to refresh we fix that by replacing "a" with "Link" and "href" with "to"
        <Card>
            {/* this will make a product to be clickable and show data for only this product */}
            <Link to={`/product/${product.slug}`}>
            <img src={product.image} className="card-img-top" alt={product.name} />
            </Link>
            <Card.Body>
                <Link to={`/product/${product.slug}`}>
                    <Card.Title><strong>{product.name}</strong></Card.Title>
                 </Link>
                 {/* Defining Rating Component  */}
                 <Rating rating={product.rating} numReviews={product.numReviews}/>
                <Card.Text>${product.price}</Card.Text>
                {
                    // If countInStock = 0 that product is finished so we disable the button 
                    product.countInStock === 0 ? (
                    <Button variant="light" disabled>
                        Out of stock
                    </Button>
                    ) : (
                    <Button onClick={() => addToCartHandler(product)}>Add to cart</Button>
                    )
                }
            </Card.Body>
        </Card>
  )
}

export default Product;