export const getError = (error) => {
    // if error.response and error.response.data.message(this one is comming from backend ) then return backend error or just return general error message 
    return error.response && error.response.data.message ? error.response.data.message : error.message;
};