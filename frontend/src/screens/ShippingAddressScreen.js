import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import { Store } from '../Store';
import CheckoutSteps from '../components/CheckoutSteps';

export default function ShippingAddressScreen() {
  const navigate = useNavigate();
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const {
    userInfo,
    cart: { shippingAddress },
  } = state;
  // Each and every input is a state If address exist get it from there otherwise empty string  We do this so that we won't loose our data 
  const [fullName, setFullName] = useState(shippingAddress.fullName || '');
  const [address, setAddress] = useState(shippingAddress.address || '');
  const [city, setCity] = useState(shippingAddress.city || '');
  const [postalCode, setPostalCode] = useState(
    shippingAddress.postalCode || ''
  );
  // If user information not exist redirect to signin otherwise shipping screen
  useEffect(() => {
    if (!userInfo) {
      navigate('/signin?redirect=/shipping');
    }
  }, [userInfo, navigate]);
  const [country, setCountry] = useState(shippingAddress.country || '');
  const submitHandler = (e) => {
      // Prevent refreshing when clicking the button 
    e.preventDefault();
    // Acc the SAVE_SHIPPING_ADDRESS Action to save the payload 
    ctxDispatch({
      type: 'SAVE_SHIPPING_ADDRESS',
      payload: {
        fullName,
        address,
        city,
        postalCode,
        country,
      },
    });
    //Then we save to Local Storage 
    localStorage.setItem(
      'shippingAddress',
      JSON.stringify({
        fullName,
        address,
        city,
        postalCode,
        country,
      })
    );
    // Then Navigate to payment screen 
    navigate('/payment');
  };
  return (
    <div>
        {/* Set the tithle of this page */}
      <Helmet>
        <title>Shipping Address</title>
      </Helmet>

      <CheckoutSteps step1 step2></CheckoutSteps>
      {/* Small Container class make the input to not fill out the screen  */}
      <div className="container small-container">
          {/* Page header  */}
        <h1 className="my-3">Shipping Address</h1>
        {/* Creating a from  */}
        <Form onSubmit={submitHandler}>
            {/* User FulName  */}
          <Form.Group className="mb-3" controlId="fullName">
            <Form.Label>Full Name</Form.Label>
            <Form.Control value={fullName} onChange={(e) => setFullName(e.target.value)} required />
          </Form.Group>
          {/* User Address  */}
          <Form.Group className="mb-3" controlId="address">
            <Form.Label>Address</Form.Label>
            <Form.Control value={address} onChange={(e) => setAddress(e.target.value)} required />
          </Form.Group>
          {/* User City  */}
          <Form.Group className="mb-3" controlId="city">
            <Form.Label>City</Form.Label>
            <Form.Control value={city} onChange={(e) => setCity(e.target.value)} required />
          </Form.Group>
          {/* User Postal Code  */}
          <Form.Group className="mb-3" controlId="postalCode">
            <Form.Label>Postal Code</Form.Label>
            <Form.Control value={postalCode} onChange={(e) => setPostalCode(e.target.value)} required />
          </Form.Group>
          {/* User Country  */}
          <Form.Group className="mb-3" controlId="country">
            <Form.Label>Country</Form.Label>
            <Form.Control value={country} onChange={(e) => setCountry(e.target.value)} required />
          </Form.Group>
          {/* Submit button  */}
          <div className="mb-3">
            <Button variant="primary" type="submit">  Continue  </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}