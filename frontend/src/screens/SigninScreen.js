import Axios from 'axios';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Helmet } from 'react-helmet-async';
import { useContext, useEffect, useState } from 'react';
import { Store } from '../Store';
import { toast } from 'react-toastify';
import { getError } from '../utils';

export default function SigninScreen() {
  const navigate = useNavigate();
  // Get user curent page or location 
    const { search } = useLocation();
 // if user location exist we will redirect in that location 
  const redirectInUrl = new URLSearchParams(search).get('redirect');
  // Otherwise redirect to Home screen 
  const redirect = redirectInUrl ? redirectInUrl : '/';

  // Email and password are state , That is why we defined them as statehook and set the dafault value to empty string 
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
// Now we know that the user exist on database the user can sign in , now we need to save this user to store and Local Storage 
  const { state, dispatch: ctxDispatch } = useContext(Store);
  // Construct user infor From State 
  const { userInfo } = state;
  // Submit function 
  const submitHandler = async (e) => {
    // Prevent the page refresh when user click sign in button 
    e.preventDefault();
    try {
      //We are sending an Ajex request to backend 
      const { data } = await Axios.post('/api/users/signin', {
        email,
        password,
      });
      // Dispatch is the action that we are doing to update the state , Action is USER_SIGNIN we are signing in with data object that contains email and password
      ctxDispatch({ type: 'USER_SIGNIN', payload: data });
      // Save the data to Localstorage from userInfo , And we convert that data to String because Local Storage save thada as string JSON.stringify(data)
      localStorage.setItem('userInfo', JSON.stringify(data));
      // After Sign In we have to redirect the User 
      navigate(redirect || '/');
    } catch (err) {
      // This error message we are getting from Backend throug utils 
      toast.error(getError(err));
    }
  };

  // We are fixing the error of a user already signed in but still able to see Sign in screen 
  useEffect(() => {
    if (userInfo) {
      navigate(redirect);
    }
  }, [navigate, redirect, userInfo]);

  return (
    <Container className="small-container">
       {/* Helmet to set the title of the page to Sign in  */}
      <Helmet>
        <title>Sign In</title>
      </Helmet>
      {/* Heading  my-3 this is setting the margin */}
      <h1 className="my-3">Sign In</h1>
      {/* This From is commint from React bootstrap */}
      <Form onSubmit={submitHandler}>
          {/* This is for email Label and Input Box  */}
        <Form.Group className="mb-3" controlId="email">
            {/* Label  */}
          <Form.Label>Email</Form.Label>
          {/* Input box for Email we set onchange and Used the hook to get password from textbox  */}
          <Form.Control type="email" required onChange={(e) => setEmail(e.target.value)}/>
        </Form.Group>
        {/* Password */}
        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" required onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>
        <div className="mb-3">
          <Button type="submit">Sign In</Button>
        </div>
        <div className="mb-3">
          New customer?{' '}
          <Link to={`/signup?redirect=${redirect}`}>Create your account</Link>
        </div>
      </Form>
    </Container>
  );
}