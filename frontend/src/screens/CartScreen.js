import { useContext } from 'react';
import { Store } from '../Store';
import { Helmet } from 'react-helmet-async';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MessageBox from '../components/MessageBox';
import ListGroup from 'react-bootstrap/ListGroup';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

export default function CartScreen() {
  const navigate = useNavigate();
    //Getting access to the context 
  const { state, dispatch: ctxDispatch } = useContext(Store);
  // From state we construct cart , from cart we construct cartItems
  const {
    cart: { cartItems },
  } = state;

  // Updating the Products Increase and Decrease QUANTITY IN THE CART   
  const updateCartHandler = async (item, quantity) => {
    // We are getting the current product by Id from the backend 
    const { data } = await axios.get(`/api/products/${item._id}`);
    if (data.countInStock < quantity) {
      window.alert('Sorry. Product is out of stock');
      return;
    }
    ctxDispatch({
      type: 'CART_ADD_ITEM',
      payload: { ...item, quantity },
    });
  };
  //Delete Item from the cart
  const removeItemHandler = (item) => {
    ctxDispatch({ type: 'CART_REMOVE_ITEM', payload: item });
  };
// Proceed to check out we are just Navigating to this screen 
  const checkoutHandler = () => {
    // We check if user is authenticated if yes we direct to Shiping screen
    navigate('/signin?redirect=/shipping');
  };

  return (
    <div>
{/* Use Helmet to set title to Shopping Cart  */}
      <Helmet>
        <title>Shopping Cart</title>
      </Helmet>
{/* Page header  */}
      <h1>Shopping Cart</h1>

      <Row>
          {/* This column we will show a list of Items  */}
        <Col md={8}>
          {
            //   If cart is empt show the message  and link to home screen 
            cartItems.length === 0 ? (
                <MessageBox>
                Cart is empty. <Link to="/">Go Shopping</Link>
                </MessageBox>
            ) : (
                // If cart not empty show list of iterms in the cart 
                <ListGroup>
                {cartItems.map((item) => (
                    <ListGroup.Item key={item._id}>
                    <Row className="align-items-center">
                        {/* First Column we show the igame with height of 80px from index.css */}
                        <Col md={4}>
                        <img
                            src={item.image}
                            alt={item.name}
                            className="img-fluid rounded img-thumbnail"
                        ></img>{' '}
                        {/* Next to Image we have a space and Link to Product */}
                        <Link to={`/product/${item.slug}`}>{item.name}</Link>
                        </Col>
                        {/* Button to decrease number of Item in the cart */}
                        <Col md={3}>
                        <Button
                        // This Click we take 2 params , 1st is the item that we want to update, 2nd is the quantity we want to decrease
                          onClick={() =>
                            updateCartHandler(item, item.quantity - 1)
                          }
                          variant="light"
                          disabled={item.quantity === 1}
                        >
                            <i className="fas fa-minus-circle"></i>
                        </Button>{' '}
                        {/* Then space and show the quantity  */}
                        <span>{item.quantity}</span>{' '}
                        {/* Another space then Button to increase quantity  */}
                        <Button
                            variant="light"
                            // Quantity we want to Increase
                            onClick={() =>
                              updateCartHandler(item, item.quantity + 1)
                            }
                            disabled={item.quantity === item.countInStock}
                        >
                            <i className="fas fa-plus-circle"></i>
                        </Button>
                        </Col>
                        {/* Price of the Item */}
                        <Col md={3}>${item.price}</Col>
                        {/* Deleting Items in the cart */}
                        <Col md={2}>
                          {/* This click function we are passing the Item that we want to remive  */}
                        <Button variant="light" onClick={() => removeItemHandler(item)}>
                            <i className="fas fa-trash"></i>
                        </Button>
                        </Col>
                    </Row>
                    </ListGroup.Item>
                ))}
                </ListGroup>
            )
          }
        </Col>
        <Col md={4}>
          <Card>
            <Card.Body>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <h3>
                    Subtotal ({cartItems.reduce((a, c) => a + c.quantity, 0)}{' '}
                    items) : $
                    {cartItems.reduce((a, c) => a + c.price * c.quantity, 0)}
                  </h3>
                </ListGroup.Item>
                <ListGroup.Item>
                  <div className="d-grid">
                    <Button
                      type="button"
                      variant="primary"
                      onClick={checkoutHandler}
                    //   if There is no Items on the cart we disable the Checkout button 
                      disabled={cartItems.length === 0}
                    >
                      Proceed to Checkout
                    </Button>
                  </div>
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}