import Axios from 'axios';
import React, { useContext, useEffect, useReducer } from 'react';
import { Helmet } from 'react-helmet-async';
import { Link, useNavigate } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import { toast } from 'react-toastify';
import { getError } from '../utils';
import { Store } from '../Store';
import CheckoutSteps from '../components/CheckoutSteps';
import LoadingBox from '../components/LoadingBox';

// We are seeting the Loading Box base on Actions 
const reducer = (state, action) => {
  switch (action.type) {
    case 'CREATE_REQUEST':
      return { ...state, loading: true };
    case 'CREATE_SUCCESS':
      return { ...state, loading: false };
    case 'CREATE_FAIL':
      return { ...state, loading: false };
    default:
      return state;
  }
};

export default function PlaceOrderScreen() {
  const navigate = useNavigate();

  // In useReducer we are getting data and dispatch Action 
  const [{ loading }, dispatch] = useReducer(reducer, {
    loading: false,
  });

//   Get the state 
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart, userInfo } = state;

  // Round the number to 2 decimal point 
  const round2 = (num) => Math.round(num * 100 + Number.EPSILON) / 100; // 123.2345 => 123.23
  cart.itemsPrice = round2(
    cart.cartItems.reduce((a, c) => a + c.quantity * c.price, 0)
  );
  cart.shippingPrice = cart.itemsPrice > 100 ? round2(0) : round2(10);
  //Tax price is 15 %  correct method 
   //cart.taxPrice = round2(0.15 * cart.itemsPrice);
  // I want to make Tax to be 0 %
    cart.taxPrice = round2(0 * cart.itemsPrice);
    //Calculate Total price 
  cart.totalPrice = cart.itemsPrice + cart.shippingPrice + cart.taxPrice;

  // Whe we click Place order Button 
  const placeOrderHandler = async () => {
    try {
      // We are dispatch this Action 
      dispatch({ type: 'CREATE_REQUEST' });
      // When we dispatch we send this Data to back end using ajax request throgh Axios
      const { data } = await Axios.post(
        '/api/orders',
        {
          orderItems: cart.cartItems,
          shippingAddress: cart.shippingAddress,
          paymentMethod: cart.paymentMethod,
          itemsPrice: cart.itemsPrice,
          shippingPrice: cart.shippingPrice,
          taxPrice: cart.taxPrice,
          totalPrice: cart.totalPrice,
        },
        // We are also sending Hearders token 
        {
          headers: {
            authorization: `Bearer ${userInfo.token}`,
          },
        }
      );
      // After sending Create we creat the cart this one does to the Store 
      ctxDispatch({ type: 'CART_CLEAR' });
      // This one goes on top CREATE_SUCCESS for loading box 
      dispatch({ type: 'CREATE_SUCCESS' });
      // Now it's time to clear the shopping cart 
      localStorage.removeItem('cartItems');
      // Then Navigate to Oder datails 
      navigate(`/order/${data.order._id}`);
    } catch (err) {
      // If Create Action Failed we display message 
      dispatch({ type: 'CREATE_FAIL' });
      toast.error(getError(err));
    }
  };


// If payment methos does not exist go back to payment method 
  useEffect(() => {
    if (!cart.paymentMethod) {
      navigate('/payment');
    }
  }, [cart, navigate]);

  return (
    <div>
        {/* In this section all the steps are Active  */}
      <CheckoutSteps step1 step2 step3 step4></CheckoutSteps>
      {/* Set title  */}
      <Helmet>
        <title>Preview Order</title>
      </Helmet>
      {/* Page Heading  */}
      <h1 className="my-3">Preview Order</h1>
      <Row>
        <Col md={8}>
          <Card className="mb-3">
            <Card.Body>
                {/* Card title  */}
              <Card.Title>Shipping</Card.Title>
              <Card.Text>
                  {/* SHow Nane  */}
                <strong>Name:</strong> {cart.shippingAddress.fullName} <br />
                {/* Show adress  */}
                <strong>Address: </strong> {cart.shippingAddress.address},
                {cart.shippingAddress.city}, {cart.shippingAddress.postalCode},
                {cart.shippingAddress.country}
              </Card.Text>
              {/* Link to edit this Iterm  */}
              <Link to="/shipping">Edit</Link>
            </Card.Body>
          </Card>
            {/* Pyment methods  */}
          <Card className="mb-3">
            <Card.Body>
              <Card.Title>Payment</Card.Title>
              <Card.Text>
                <strong>Method:</strong> {cart.paymentMethod}
              </Card.Text>
              <Link to="/payment">Edit</Link>
            </Card.Body>
          </Card>
        {/* List of iterms in the cart  */}
          <Card className="mb-3">
            <Card.Body>
              <Card.Title>Items</Card.Title>
              <ListGroup variant="flush">
                  {/* Loop that is looping through each Iterms  */}
                {cart.cartItems.map((item) => (
                  <ListGroup.Item key={item._id}>
                    <Row className="align-items-center">
                      <Col md={6}>
                        <img
                          src={item.image}
                          alt={item.name}
                          className="img-fluid rounded img-thumbnail"
                        ></img>{' '}
                        <Link to={`/product/${item.slug}`}>{item.name}</Link>
                      </Col>
                      <Col md={3}>
                        <span>{item.quantity}</span>
                      </Col>
                      <Col md={3}>${item.price}</Col>
                    </Row>
                  </ListGroup.Item>
                ))}
              </ListGroup>
              <Link to="/cart">Edit</Link>
            </Card.Body>
          </Card>
        </Col>
        {/* Order Summary  */}
        <Col md={4}>
          <Card>
            <Card.Body>
              <Card.Title>Order Summary</Card.Title>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <Row>
                    <Col>Items</Col>
                    <Col>${cart.itemsPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Shipping</Col>
                    <Col>${cart.shippingPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Tax</Col>
                    <Col>${cart.taxPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>
                      <strong> Order Total</strong>
                    </Col>
                    <Col>
                      <strong>${cart.totalPrice.toFixed(2)}</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <div className="d-grid">
                    <Button
                      type="button"
                      onClick={placeOrderHandler}
                      disabled={cart.cartItems.length === 0}
                    >
                      Place Order
                    </Button>
                  </div>
                  {loading && <LoadingBox></LoadingBox>}
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}