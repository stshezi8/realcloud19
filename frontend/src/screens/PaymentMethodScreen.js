import React, { useContext, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CheckoutSteps from '../components/CheckoutSteps';
import { Store } from '../Store';

export default function PaymentMethodScreen() {
  const navigate = useNavigate();
  // We have to get the state 
  const { state, dispatch: ctxDispatch } = useContext(Store);
//   Get cart 
  const {
    cart: { shippingAddress, paymentMethod },
  } = state;

  // State for payment 
  const [paymentMethodName, setPaymentMethod] = useState(
    paymentMethod || 'PayPal'
  );
// If shipping address does not exist navigate user to previous step 
  useEffect(() => {
    if (!shippingAddress.address) {
      navigate('/shipping');
    }
  }, [shippingAddress, navigate]);

  const submitHandler = (e) => {
    e.preventDefault();
    // Dispatching this action 
    ctxDispatch({ type: 'SAVE_PAYMENT_METHOD', payload: paymentMethodName });
    // Save to local storage 
    localStorage.setItem('paymentMethod', paymentMethodName);
    // Redirect the user to placeholder  
    navigate('/placeorder');
  };
  return (
    <div>
        {/* Step 1 to 3 are active in this step  */}
      <CheckoutSteps step1 step2 step3></CheckoutSteps>
      {/* Make the screen bit smaller  */}
      <div className="container small-container">
          {/* Set title of the page  */}
        <Helmet>
          <title>Payment Method</title>
        </Helmet>
        {/* Page heading  */}
        <h1 className="my-3">Payment Method</h1>
        {/* Form  */}
        <Form onSubmit={submitHandler}>
          <div className="mb-3">
            <Form.Check type="radio" id="PayPal" label="PayPal" value="PayPal" checked={paymentMethodName === 'PayPal'} onChange={(e) => setPaymentMethod(e.target.value)} />
          </div>
          <div className="mb-3">
            <Form.Check type="radio" id="Stripe" label="Stripe" value="Stripe"  checked={paymentMethodName === 'Stripe'} onChange={(e) => setPaymentMethod(e.target.value)} />
          </div>
          <div className="mb-3">
            <Button type="submit">Continue</Button>
          </div>
        </Form>
      </div>
    </div>
  );
}