import { useEffect, useReducer, useState } from "react";
import axios from 'axios';
import logger from 'use-reducer-logger';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import Product from "../components/Product";
import { Helmet } from "react-helmet-async";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
// import data from "../data";

//Reducer function accept 2 parameters first is the current state and action that chnage the state and put the new state
const reducer = (state,action) => {
  //If action type is fetch/request data from api we keep the current state and set loading to true just to show loading box to the UI
  switch(action.type){
      case 'FETCH_REQUEST':
          return {...state,loading:true};
   //If the action type is successfuly got data we return new state , and update product with data that is comming from action.payload (backend) and update loading to false     
      case 'FETCH_SUCCESS':
          return {...state, products:action.payload,loading:false};
   // If the fetch is failed keep the curent state , set load to false and update error with the error that is on action.payload
       case 'FETCH_FAIL':
           return{...state, loading:false, error: action.payload};
    // If action type is not on the above just return the curent state
        default:
            return state;
  }
}

function HomeScreen() {
  // It's time to define the state to save the product from backend, useState is the Hook from react and set the default to empty array 
  // Usestate Hook is a function that returns an array that has a variable "products" and function "setProducts" that update the variable "products"
  // To get more information about state hook : https://reactjs.org/docs/hooks-state.html
  // const [products, setProducts] = useState([]);

   //Now it's time to use the reducer in Home screen
   // We define an array that contais 2 parameters first one {..}, second is dispatch to call a function that update the state 
   //useReducer takes 2 parameters first is the one we created above reducer, 2nd is default state , we will create an object and set it to true, because when we render this component we will fetch products from back end
   const [{loading,error,products}, dispatch] = useReducer(logger(reducer), {
    //We are seting the default value for products to empty array
    products:[],loading:true , error: '',
});



  // Defining another Hook is effect , This is a function that accept 2 parameters , 1st is the function , 2nd is the Array 
  useEffect(() => {
    // It's time to use dispatch to update the state
    dispatch({ type:'FETCH_REQUEST'});
    // Call the api with fetchData function that is async that takes no parameters
    const fetchData = async () => {
      try{
            // We define a variable result then call axios.get method to send the Ajax request to this address and put the result in the variable result
            const result = await axios.get('/api/products');
            // If I successfully got data from backend I have to dispatch Success 
            dispatch({type:'FETCH_SUCCESS',payload:result.data})
          }catch(error){
              dispatch({type:'FETCH_FAIL', payload: error.message})
          }
    };
    // Commit 
    fetchData();
},[]);

   return <div>
     <Helmet>
       <title>Cloud19</title>
     </Helmet>
       <h1>Feature Products</h1>
           {/* Data is the object withing that there is another Object called products.
               We need to use map function to convert each product to jsx
               Console error Warning: Each child in a list should have a unique "key" prop.
               to fix it key=product.slug  to make each product unique */}
               {/* After Defining useEffect(() then it's time to remove data and get data from API */}
           <div className="products">
           {
             loading ? (
              //  we are replacing this with loading component
                  <LoadingBox/>
                ) : error ? (
                  // Here the variant is Danger so in the messagebox component it will pass danger
                    <MessageBox variant="danger">{error}</MessageBox>
                ) : (
                  <Row>
                    {products.map((product) => (
                        // For small scell we will have 2 products , Medium we will have 3 , for big screen we will have 4 
                        <Col key={product.slug} sm={6} md={4} lg={3}>
                          <Product  product={product}></Product>
                        </Col>
                      ))}
                    </Row>
                )
             }
           </div>
   </div>;
}
export default HomeScreen;
