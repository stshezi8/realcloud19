import axios from 'axios';
import React, { useContext, useEffect, useReducer } from 'react';
import { PayPalButtons, usePayPalScriptReducer } from '@paypal/react-paypal-js';
import { Helmet } from 'react-helmet-async';
import { useNavigate, useParams } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { Store } from '../Store';
import { getError } from '../utils';
import { toast } from 'react-toastify';

// We are implementing our actions 
function reducer(state, action) {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true, error: '' };
    case 'FETCH_SUCCESS':
      return { ...state, loading: false, order: action.payload, error: '' };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    case 'PAY_REQUEST':
      return { ...state, loadingPay: true };
    case 'PAY_SUCCESS':
      return { ...state, loadingPay: false, successPay: true };
    case 'PAY_FAIL':
      return { ...state, loadingPay: false };
    case 'PAY_RESET':
      // This one we are reseting loading pay and successpay to the default state .
      return { ...state, loadingPay: false, successPay: false };

    case 'DELIVER_REQUEST':
      return { ...state, loadingDeliver: true };
    case 'DELIVER_SUCCESS':
      return { ...state, loadingDeliver: false, successDeliver: true };
    case 'DELIVER_FAIL':
      return { ...state, loadingDeliver: false };
    case 'DELIVER_RESET':
      return { ...state, loadingDeliver: false, successDeliver: false, };
      
    default:
      return state;
  }
}
export default function OrderScreen() {
    // User information is comming from Store context 
  const { state } = useContext(Store);
  const { userInfo } = state;

  // Getting user ID from the url and store it to orderId
  const params = useParams();
  const { id: orderId } = params;

  // From react dom used to navigage between pages 
  const navigate = useNavigate();

// We are getting a loading , error , order and dispatch from useReducer, by Default , Loading is true , order is empty and error is empty 
const [ { loading, error, order, successPay, loadingPay, loadingDeliver, successDeliver, },
  dispatch,
] = useReducer(reducer, {
  loading: true,
  order: {},
  error: '',
  successPay: false,
  loadingPay: false,
});

// Paypal respatch that return state of loading screan and function to load the script 
const [{ isPending }, paypalDispatch] = usePayPalScriptReducer();

//This function takes 2 parameters 
function createOrder(data, actions) {
return actions.order
// We are calling a create function in actions.order Object 
  .create({
    purchase_units: [
      {
        // And passing the Amount based on order.totalPrice
        amount: { value: order.totalPrice },
      },
    ],
  })
  .then((orderID) => {
    // If it Creates this order successfuly then it returns this Order from PAYPAL 
    return orderID;
  });
}
// This function happening after successfull of capturing the payments 
function onApprove(data, actions) {
return actions.order.capture().then(async function (details) {
  // Now we have to update our order in the back end 
  try {
    dispatch({ type: 'PAY_REQUEST' });
    // On back end we have to update this api 
    const { data } = await axios.put(
      `/api/orders/${order._id}/pay`,
      // Details caontains user information and payment information from paypal 
      details,
      {
        headers: { authorization: `Bearer ${userInfo.token}` },
      }
    );
    // Dispatch success 
    dispatch({ type: 'PAY_SUCCESS', payload: data });
    // Show this information to the use The order is payed 
    toast.success('Order is paid');
  } catch (err) {
    // If there is an error show the error pay FAIL 
    dispatch({ type: 'PAY_FAIL', payload: getError(err) });
    // We are using toast to show error in the user 
    toast.error(getError(err));
  }
});
}
function onError(err) {
toast.error(getError(err));
}

  // Get Order information from Back end 
  useEffect(() => {
    const fetchOrder = async () => {
      try {
        // Dispatching FETCH_REQUEST  for loading screen
        dispatch({ type: 'FETCH_REQUEST' });
        const { data } = await axios.get(`/api/orders/${orderId}`, {
          headers: { authorization: `Bearer ${userInfo.token}` },
        });
        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      } catch (err) {
        dispatch({ type: 'FETCH_FAIL', payload: getError(err) });
      }
    };

    // We check use information if it's null direct user to Login page 
    if (!userInfo) {
      return navigate('/login');
    }
    if ( !order._id || successPay || successDeliver || (order._id && order._id !== orderId) ) {
      fetchOrder();
      if (successPay) {
        dispatch({ type: 'PAY_RESET' });
      }
      if (successDeliver) {
        dispatch({ type: 'DELIVER_RESET' });
      }
    } else {
      const loadPaypalScript = async () => {
        // We are getting a paypal ID from the backend with this Ajax Request 
        const { data: clientId } = await axios.get('/api/keys/paypal', {
          headers: { authorization: `Bearer ${userInfo.token}` },
        });
        // We are setting the ID that we get from back end and Curreny to USD 
        paypalDispatch({
          type: 'resetOptions',
          value: {
            'client-id': clientId,
            currency: 'USD',
          },
        });
        paypalDispatch({ type: 'setLoadingStatus', value: 'pending' });
      };
      loadPaypalScript();
    }
  }, [ order, userInfo, orderId, navigate, paypalDispatch, successPay, successDeliver, ]);

  async function deliverOrderHandler() {
    try {
      dispatch({ type: 'DELIVER_REQUEST' });
      const { data } = await axios.put(
        `/api/orders/${order._id}/deliver`,
        {},
        {
          headers: { authorization: `Bearer ${userInfo.token}` },
        }
      );
      dispatch({ type: 'DELIVER_SUCCESS', payload: data });
      toast.success('Order is delivered');
    } catch (err) {
      toast.error(getError(err));
      dispatch({ type: 'DELIVER_FAIL' });
    }
  }
  // If it's Loading show loading box
  return loading ? (
    <LoadingBox></LoadingBox>
  ) : error ? (
    //   if there is an error show error message 
    <MessageBox variant="danger">{error}</MessageBox>
  ) : (
    //   Otherwise show Order
    <div>
        {/* Page Title  */}
      <Helmet>
        <title>Order {orderId}</title>
      </Helmet>

      {/* Page Header  */}
      <h1 className="my-3">Order {orderId}</h1>
      <Row>
        <Col md={8}>
            {/* First Card show Shipping information  */}
          <Card className="mb-3">  
            <Card.Body>
              <Card.Title>Shipping</Card.Title>
              <Card.Text>
                <strong>Name:</strong> {order.shippingAddress.fullName} <br />
                <strong>Address: </strong> {order.shippingAddress.address},
                {order.shippingAddress.city}, {order.shippingAddress.postalCode}
                ,{order.shippingAddress.country}
              </Card.Text>
              {order.isDelivered ? (
                <MessageBox variant="success">
                  Delivered at {order.deliveredAt}
                </MessageBox>
              ) : (
                <MessageBox variant="danger">Not Delivered</MessageBox>
              )}
            </Card.Body>
          </Card>
          {/* Payment information  */}
          <Card className="mb-3">
            <Card.Body>
              <Card.Title>Payment</Card.Title>
              <Card.Text>
                <strong>Method:</strong> {order.paymentMethod}
              </Card.Text>
              {/* If order is payed it shows payed at this date  */}
              {order.isPaid ? (
                <MessageBox variant="success">
                  Paid at {order.paidAt}
                </MessageBox>
              ) : (
                //   otherwise not paid 
                <MessageBox variant="danger">Not Paid</MessageBox>
              )}
            </Card.Body>
          </Card>

          <Card className="mb-3">
              {/* Order Items  */}
            <Card.Body>
              <Card.Title>Items</Card.Title>
              <ListGroup variant="flush">
                {order.orderItems.map((item) => (
                  <ListGroup.Item key={item._id}>
                    <Row className="align-items-center">
                      <Col md={6}>
                        <img
                          src={item.image}
                          alt={item.name}
                          className="img-fluid rounded img-thumbnail"
                        ></img>{' '}
                        <Link to={`/product/${item.slug}`}>{item.name}</Link>
                      </Col>
                      <Col md={3}>
                        <span>{item.quantity}</span>
                      </Col>
                      <Col md={3}>${item.price}</Col>
                    </Row>
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4}>
            {/* Order Summary  */}
          <Card className="mb-3">
            <Card.Body>
              <Card.Title>Order Summary</Card.Title>
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <Row>
                    <Col>Items</Col>
                    <Col>${order.itemsPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Shipping</Col>
                    <Col>${order.shippingPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Tax</Col>
                    <Col>${order.taxPrice.toFixed(2)}</Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>
                      <strong> Order Total</strong>
                    </Col>
                    <Col>
                      <strong>${order.totalPrice.toFixed(2)}</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                  {/* If order is not payed we need to show  PAYPAL button */}
                {!order.isPaid && (
                  <ListGroup.Item>
                    {isPending ? (
                      <LoadingBox />
                    ) : (
                      <div>
                        <PayPalButtons
                        // Create order handler run when you click on PAYPAL BUTTON 
                          createOrder={createOrder}
                          //On Approver run when you have a successful payment to update the status on the order in the backend 
                          onApprove={onApprove}
                          //When there is an error in paying the Order 
                          onError={onError}
                        ></PayPalButtons>
                      </div>
                    )}
                    {loadingPay && <LoadingBox></LoadingBox>}
                  </ListGroup.Item>
                )}
                {/* If user is Admin, Order is paid but order is not Delivered then show this button to deliver order */}
                {userInfo.isAdmin && order.isPaid && !order.isDelivered && (
                  <ListGroup.Item>
                    {loadingDeliver && <LoadingBox></LoadingBox>}
                    <div className="d-grid">
                      <Button type="button" onClick={deliverOrderHandler}>
                        Deliver Order
                      </Button>
                    </div>
                  </ListGroup.Item>
                )}
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}