import React, { useContext, useEffect, useReducer } from 'react';
import { Helmet } from 'react-helmet-async';
import Chart from 'react-google-charts';
import axios from 'axios';
import { Store } from '../Store';
import { getError } from '../utils';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

// Defining the reducer 
const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      // When we got data from back end we are storing that data to summary
      return { ...state, summary: action.payload, loading: false, };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
export default function DashboardScreen() {
  // It's time to use the ruducer hook . dispatch to call the Action type and update the state of reducer 
  const [{ loading, summary, error }, dispatch] = useReducer(reducer, {
    loading: true,
    error: '',
  });

  // Get User Info Store 
  const { state } = useContext(Store);
  // Get userInfo from State 
  const { userInfo } = state;

  // Use useEffect to get Dashboard data 
  useEffect(() => {
    const fetchData = async () => {
      try {
        // sending Ajax request using axios to backend api /api/orders/summary 
        const { data } = await axios.get('/api/orders/summary', {
          //By this code we can authenticate to this API 
          headers: { Authorization: `Bearer ${userInfo.token}` },
        });
        // on suucess we fill payload with data from backend  then on reducer we will summary with payload data 
        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      } catch (err) {
        dispatch({
          type: 'FETCH_FAIL',
          payload: getError(err),
        });
      }
    };
    fetchData();
  }, [userInfo]);

  return (
    <div>
       {/* Set Header of the page  */}
       <Helmet>
        <title>Admin Dashboard</title>
      </Helmet>

      {/* Page heading  */}
      <h1>Dashboard</h1>

      {loading ? (
        //If loading show loading box 
        <LoadingBox />
      ) : error ? (
        // If error show error 
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <>
        {/* Otherwise show data  */}
          <Row>
            <Col md={4}>
              {/* All Users registed in our website  */}
              <Card>
                <Card.Body>
                  <Card.Title>
                    {summary.users && summary.users[0]
                      ? summary.users[0].numUsers
                      : 0}
                  </Card.Title>
                  <Card.Text> Users</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            {/* Number of orders  */}
            <Col md={4}>
              <Card>
                <Card.Body>
                  <Card.Title>
                    {summary.orders && summary.users[0]
                      ? summary.orders[0].numOrders
                      : 0}
                  </Card.Title>
                  <Card.Text> Orders</Card.Text>
                </Card.Body>
              </Card>
            </Col>
            {/* Total Sales  */}
            <Col md={4}>
              <Card>
                <Card.Body>
                  <Card.Title>
                    $
                    {summary.orders && summary.users[0]
                      ? summary.orders[0].totalSales.toFixed(2)
                      : 0}
                  </Card.Title>
                  <Card.Text> Total Sales</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          {/* We will display the chart  */}
          <div className="my-3">
            <h2>Sales</h2>
            {/* If there is no sales we have to show this message  */}
            {summary.dailyOrders.length === 0 ? (
              <MessageBox>No Sale</MessageBox>
            ) : (
              // If the is sales we show the Chart 
              <Chart
                width="100%"
                height="400px"
                // Chart type 
                chartType="AreaChart"
                loader={<div>Loading Chart...</div>}
                //We have 2 arrays First show Date and sales , 2nd shows data use Map function to loop through summary.dailyOrders
                data={[
                  ['Date', 'Sales'],
                  ...summary.dailyOrders.map((x) => [x._id, x.sales]),
                ]}
              ></Chart>
            )}
          </div>
          {/* Categories  */}
          <div className="my-3">
            <h2>Categories</h2>
            {summary.productCategories.length === 0 ? (
              <MessageBox>No Category</MessageBox>
            ) : (
              <Chart
                width="100%"
                height="400px"
                chartType="PieChart"
                loader={<div>Loading Chart...</div>}
                data={[
                  ['Category', 'Products'],
                  ...summary.productCategories.map((x) => [x._id, x.count]),
                ]}
              ></Chart>
            )}
          </div>
        </>
      )}
    </div>
  );
}