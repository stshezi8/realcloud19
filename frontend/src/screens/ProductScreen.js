import axios from "axios";
import { useContext, useEffect, useReducer, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';
import Rating from "../components/Rating";
import Button from "react-bootstrap/esm/Button";
import { Helmet } from "react-helmet-async";
import LoadingBox from "../components/LoadingBox";
import MessageBox from "../components/MessageBox";
import { getError } from "../utils";
import { Store } from "../Store";
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import { toast } from 'react-toastify';

const reducer = (state,action) => {
    switch(action.type){
        case 'REFRESH_PRODUCT':
        return { ...state, product: action.payload };
        case 'CREATE_REQUEST':
        return { ...state, loadingCreateReview: true };
        case 'CREATE_SUCCESS':
        return { ...state, loadingCreateReview: false };
        case 'CREATE_FAIL':
        return { ...state, loadingCreateReview: false };
        case 'FETCH_REQUEST':
            return {...state,loading:true};
        case 'FETCH_SUCCESS':
            return {...state, product:action.payload,loading:false};
         case 'FETCH_FAIL':
             return{...state, loading:false, error: action.payload};
          default:
              return state;
    }
  }
function ProductScreen() {

    let reviewsRef = useRef();

    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState('');
    const [selectedImage, setSelectedImage] = useState('');

    // Defining useNavigate Hook
    const navigate = useNavigate();
    const params = useParams();
    const {slug} = params;
    const [{ loading, error, product, loadingCreateReview }, dispatch] =  useReducer(reducer, {
      product: [],
      loading: true,
      error: '',
    });

      useEffect(() => {
        dispatch({ type:'FETCH_REQUEST'});
        const fetchData = async () => {
          try{
                const result = await axios.get(`/api/products/slug/${slug}`);
                dispatch({type:'FETCH_SUCCESS',payload:result.data})
              }catch(error){
                  dispatch({type:'FETCH_FAIL', payload: getError(error)})
              }
        };
        // Commit 
        fetchData();
    },[slug]);// When the user click on the new product , slug will be updated and get a new product 

    //To Add Iterms to the cart we need to dispatch an action onthe react context , So we need to get the context 
    //By using useContext we have access to the state and (dispatch)Action to chnage the state 
    const { state, dispatch: ctxDispatch } = useContext(Store);
    // Berofe we add item to cart we need to have cart in the start
    const { cart , userInfo } = state;
    // addToCartHandler is a function that add iterms to the cart 
    const addToCartHandler = async () => {
        //Current product exist in the cart or not 
        const existItem = cart.cartItems.find((x) => x._id === product._id);
        // If it exist we have to increase the quantity by1 
        const quantity = existItem ? existItem.quantity + 1 : 1;
        //We can't add products that are more than the products that we have in stock 
        const { data } = await axios.get(`/api/products/${product._id}`);
        if (data.countInStock < quantity) {
        window.alert('Sorry. Product is out of stock');
        return;
        }
      ctxDispatch({
        type: 'CART_ADD_ITEM',
        payload: { ...product, quantity },
      });
    //   after Dispatching we direct user to Cart screen
      navigate('/cart');
    };

    const submitHandler = async (e) => {
        e.preventDefault();
        if (!comment || !rating) {
          toast.error('Please enter comment and rating');
          return;
        }
        try {
          const { data } = await axios.post(
            `/api/products/${product._id}/reviews`,
            { rating, comment, name: userInfo.name },
            {
              headers: { Authorization: `Bearer ${userInfo.token}` },
            }
          );
    
          dispatch({
            type: 'CREATE_SUCCESS',
          });
          toast.success('Review submitted successfully');
          product.reviews.unshift(data.review);
          product.numReviews = data.numReviews;
          product.rating = data.rating;
          dispatch({ type: 'REFRESH_PRODUCT', payload: product });
          window.scrollTo({
            behavior: 'smooth',
            top: reviewsRef.current.offsetTop,
          });
        } catch (error) {
          toast.error(getError(error));
          dispatch({ type: 'CREATE_FAIL' });
        }
      };

    return loading ? (
        //  we are replacing this with loading component
            <LoadingBox/>
          ) : error ? (
            // Here the variant is Danger so in the messagebox component it will pass danger
              <MessageBox variant="danger">{error}</MessageBox>
    ):(
        //Showing product information 
        <div>
            <Row>
                <Col md={6}>
                    <img className="img-large" src={selectedImage || product.image} alt={product.name}/>
                </Col>
                <Col md={3}>
                    <ListGroup variant="flush">
                        <ListGroup.Item >
                            <Helmet>
                                <title>{product.name}</title>
                            </Helmet>
                            <h1>{product.name}</h1>
                        </ListGroup.Item>
                        <ListGroup.Item >
                          <Rating rating={product.rating} numReviews={product.numReviews}></Rating>
                        </ListGroup.Item>
                        <ListGroup.Item >
                            Price : ${product.price}
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row xs={1} md={2} className="g-2">
                                {[product.image, ...product.images].map((x) => (
                                <Col key={x}>
                                    <Card>
                                    <Button
                                        className="thumbnail"
                                        type="button"
                                        variant="light"
                                        onClick={() => setSelectedImage(x)}
                                    >
                                        <Card.Img variant="top" src={x} alt="product" />
                                    </Button>
                                    </Card>
                                </Col>
                                ))}
                            </Row>
                        </ListGroup.Item>
                        
                        <ListGroup.Item >
                            Descriptiom : ${product.description}
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md={3}>
                    <Card>
                        <Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>
                                    <Row>
                                        <Col>Price : </Col>
                                        <Col>${product.price}</Col>
                                    </Row>
                                </ListGroup.Item>
                                <ListGroup.Item>
                                    <Row>
                                        <Col>Status:</Col>
                                        <Col>
                                            {product.countInStock > 0 ? (
                                                <Badge bg="success">In Stock</Badge>
                                            ):(
                                                <Badge bg="danger">Unavailable</Badge>
                                            )}
                                        </Col>
                                    </Row>
                                </ListGroup.Item>
                                {product.countInStock > 0 && (
                                    <ListGroup.Item>
                                        <div className="d-grid">
                                            <Button onClick={addToCartHandler} variant="primary"> Add to Cart</Button>
                                        </div>
                                    </ListGroup.Item>
                                )}
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <div className="my-3">
                <h2 ref={reviewsRef}>Reviews</h2>
                {/* If there is no Reviews we will show this message  */}
                <div className="mb-3">
                {product.reviews.length === 0 && (
                    <MessageBox>There is no review</MessageBox>
                )}
                </div>
                {/* If there is reviews we will show them  */}
                <ListGroup>
                {product.reviews.map((review) => (
                    <ListGroup.Item key={review._id}>
                    <strong>{review.name}</strong>
                    <Rating rating={review.rating} caption=" "></Rating>
                    <p>{review.createdAt.substring(0, 10)}</p>
                    <p>{review.comment}</p>
                    </ListGroup.Item>
                ))}
                </ListGroup>
                {/* Form to do reviews  */}
                <div className="my-3">
                    {/* if the user exist must do review  */}
                {userInfo ? (
                    <form onSubmit={submitHandler}>
                    <h2>Write a customer review</h2>
                    {/* Drop down of Ratings  */}
                    <Form.Group className="mb-3" controlId="rating">
                        <Form.Label>Rating</Form.Label>
                        <Form.Select
                        aria-label="Rating"
                        value={rating}
                        onChange={(e) => setRating(e.target.value)}
                        >
                        <option value="">Select...</option>
                        <option value="1">1- Poor</option>
                        <option value="2">2- Fair</option>
                        <option value="3">3- Good</option>
                        <option value="4">4- Very good</option>
                        <option value="5">5- Excelent</option>
                        </Form.Select>
                    </Form.Group>
                    {/* Text Area of coment  */}
                    <FloatingLabel
                        controlId="floatingTextarea"
                        label="Comments"
                        className="mb-3"
                    >
                        <Form.Control
                        as="textarea"
                        placeholder="Leave a comment here"
                        value={comment}
                        onChange={(e) => setComment(e.target.value)}
                        />
                    </FloatingLabel>

                    <div className="mb-3">
                        <Button disabled={loadingCreateReview} type="submit">
                        Submit
                        </Button>
                        {loadingCreateReview && <LoadingBox></LoadingBox>}
                    </div>
                    </form>
                ) : (
                    <MessageBox>
                    Please{' '}
                    {/* if the user does not exist the user must sign in  */}
                    <Link to={`/signin?redirect=/product/${product.slug}`}>
                        Sign In
                    </Link>{' '}
                    to write a review
                    </MessageBox>
                )}
                </div>
            </div>

        </div>
    );
 }
 export default ProductScreen;