import React, { useContext, useEffect, useReducer } from 'react';
import { Helmet } from 'react-helmet-async';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { Store } from '../Store';
import { getError } from '../utils';
import Button from 'react-bootstrap/esm/Button';

// Defining the resuder that we will use in useReducer
const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, orders: action.payload, loading: false };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default function OrderHistoryScreen() {
    // Get user information from the context 
  const { state } = useContext(Store);
  const { userInfo } = state;
  const navigate = useNavigate();

  // Define useReducer
  const [{ loading, error, orders }, dispatch] = useReducer(reducer, {
    loading: true,
    error: '',
  });

// Fetch data from back end 
  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const { data } = await axios.get(
          `/api/orders/mine`,

          { headers: { Authorization: `Bearer ${userInfo.token}` } }
        );
        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      } catch (error) {
        dispatch({
          type: 'FETCH_FAIL',
          payload: getError(error),
        });
      }
    };
    // Call fetchData function 
    fetchData();
  }, [userInfo]);


  return (
    <div>
        {/* Set Header of the page  */}
      <Helmet>
        <title>Order History</title>
      </Helmet>
        {/* Page heaher  */}
      <h1>Order History</h1>
      {loading ? (
        //   If Loading show loading Box 
        <LoadingBox></LoadingBox>
      ) : error ? (
        //   If there is error show errors 
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        //   Otherwise show table 
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>DATE</th>
              <th>TOTAL</th>
              <th>PAID</th>
              <th>DELIVERED</th>
              <th>ACTIONS</th>
            </tr>
          </thead>
          <tbody>
            {
                //Order .map to loop or render Orders in the screen 
                orders.map((order) => (
                <tr key={order._id}>
                    <td>{order._id}</td>
                    {/* We are using substring to show Date and not show time part of the date  */}
                    <td>{order.createdAt.substring(0, 10)}</td>
                    <td>{order.totalPrice.toFixed(2)}</td>
                    {/* If the order is paid show date of payment otherwise just show No  */}
                    <td>{order.isPaid ? order.paidAt.substring(0, 10) : 'No'}</td>
                    {/* If it's delivered show date otherwise show No  */}
                    <td>{order.isDelivered? order.deliveredAt.substring(0, 10): 'No'}</td>
                    <td>
                        {/* Show order details by navigation to order/${order._id} */}
                    <Button type="button" variant="light" onClick={() => { navigate(`/order/${order._id}`); }} >
                        Details
                    </Button>
                    </td>
                </tr>
                ))
            }
          </tbody>
        </table>
      )}
    </div>
  );
}