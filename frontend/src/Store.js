import { createContext, useReducer } from 'react';
//First cline is to vreate a store 
export const Store = createContext();

// From the Begining there is no item from the cart that is why cart item is empty 
const initialState = {
// Check local storage for userInfo 
  userInfo: localStorage.getItem('userInfo')
  // If it does exist use JSON.parse to convert userInfo from String to Javascript Object
    ? JSON.parse(localStorage.getItem('userInfo'))
    // Other wise set a default value to null 
    : null,
  cart: {
    // If Shipping adress exist save it to Local Storage if not Keep an empty object 
      shippingAddress: localStorage.getItem('shippingAddress')
      ? JSON.parse(localStorage.getItem('shippingAddress'))
      : {},
      // Payment If it does exist on local storage use it otherwise make it empty string  
      paymentMethod: localStorage.getItem('paymentMethod')
      ? localStorage.getItem('paymentMethod')
      : '',

    // if iterms exist in local storage
      cartItems: localStorage.getItem('cartItems')
      ? JSON.parse(localStorage.getItem('cartItems'))// Use this to convert to javascript object 
      : [],// Otherwise set it to empty array 
  },
};

function reducer(state, action) {
  switch (action.type) {
    case 'CART_ADD_ITEM':
      // store the product that we gonna add to the cart to newItem
      const newItem = action.payload;
      //Get existItem, base on criteria we difined on ProductScreen 
      const existItem = state.cart.cartItems.find(
        (item) => item._id === newItem._id
      );
      //We use map function to update the current Item with newItem otherwise if new item is null newItem : item we have to add that Item at the end of array
      const cartItems = existItem ? state.cart.cartItems.map((item) =>
            item._id === existItem._id ? newItem : item
          ) : [...state.cart.cartItems, newItem];
          // We want to add Items to Local storage so that when we refresh the page we won't loose things in the cart 
          // We use JSON.stringify to convert cartItems to string 
          localStorage.setItem('cartItems', JSON.stringify(cartItems));
      return { ...state, cart: { ...state.cart, cartItems } };

    case 'CART_REMOVE_ITEM': {
      // use Filter on cart cartItems Array
        const cartItems = state.cart.cartItems.filter(
          // If item ID is not equal to current ID return it otherwise remove it 
          (item) => item._id !== action.payload._id
        );
        //Remove from Local Storage 
        localStorage.setItem('cartItems', JSON.stringify(cartItems));
        return { ...state, cart: { ...state.cart, cartItems } };
    }
    
    case 'CART_CLEAR':
      return { ...state, cart: { ...state.cart, cartItems: [] } };

    // Action for user Sign in 
    case 'USER_SIGNIN':
      // ...state keep the previous state . userInfo: action.payload Update the userInfo with the Data that we get from Backend 
      return { ...state, userInfo: action.payload };
    case 'USER_SIGNOUT':
      // We keep previous state , and set userInfo to Null 
      return { ...state, userInfo: null,
        // When we sign out we also clear  cartItems  and shippingAddress
        cart: { cartItems: [],  shippingAddress: {}, paymentMethod: '',  },
      };
    case 'SAVE_SHIPPING_ADDRESS':
      return { ...state,
        cart: { ...state.cart, shippingAddress: action.payload, },
      };
      case 'SAVE_PAYMENT_METHOD':
      return { ...state, cart: { ...state.cart, paymentMethod: action.payload },
      };
    default:
      return state;
  }
}

export function StoreProvider(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  // We have the current state , dispatch is the actions that is updating the value of a state 
  const value = { state, dispatch };
  return <Store.Provider value={value}>{props.children} </Store.Provider>;
}